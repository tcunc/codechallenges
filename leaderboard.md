## Code Challenge Leaderboard
For every challenge that you complete on time, you will gain a point. Some challenges may potentially have bonus points available (will be specified in the challenge itself).

## Users

|  Username      | Score |
| -------------- |:-----:|
| eragon5779     | 9     |
| TylerAdamsUNCO | 2     |
| ItsMatisyahu   | 0     |
| gbobo1997      | 2     |
| splaterdodge   | 1     |
| mshortle       | 0     |
| kepler3        | 3     |

## Challenges

| Challenge            | Submissions | Top Submission |
|:--------------------:|:-----------:|:--------------:|
| Chinese Zodiac       | 4           | [gbobo1997](https://gitlab.com/tcunc/codechallenges/blob/master/Submissions/gbobo1997/Chinese%20Zodiac/Main.java) |
| Harshad Series       | 2           | [gbobo1997](https://gitlab.com/tcunc/codechallenges/blob/master/Submissions/gbobo1997/HashadSeries/main.java) |
| SHA-1 Implementation | 1           | [eragon5779](https://gitlab.com/tcunc/codechallenges/blob/master/Submissions/eragon5779/SHA-1/Dlang/sha1.d) |
| Metronome            | 1           | [eragon5779](https://gitlab.com/tcunc/codechallenges/blob/master/Submissions/eragon5779/Metronome/C#/) |
| Guess the Number     | 3           | [kepler3](https://gitlab.com/tcunc/codechallenges/blob/master/Submissions/kepler3/Guess%20The%20Number/) |


## Top Submissions

Submissions are judged on the following:

* Functional code
* Cleanliness
* Optimizations
* Error Handling

| Challenge          | Submitter  | Language | LOC |
|:------------------:|:----------:|:--------:|:---:|
| Chinese Zodiac     | gbobo1997  | Java     | 29  |     
| Harshad Series     | gbobo1997  | Java     | 47  |
| SHA-1 Implentation | eragon5779 | Dlang    | 81  |
| Metronome          | eragon5779 | C#       | 370 |
| Guess the Number   | kepler3    | Java     | 160 |

## Notes

If multiple submissions are made by a single user, only 1 submission will be counted, and the higher scoring submission will be added to your score.

However, ***all submissions*** will be looked at when finding the Top Submission.

Submissions will be judged by Casey (eragon5779), and as such, he is barred from getting a Top Submission ***unless there are no other submissions***.
