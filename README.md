# CodeChallenges

TCUNC Weekly Code Challenges

## What is this

This is a code challenge for all of the programmers in the TCUNC to maintain their skills.

Challenges will be posted weekly for people to attempt, and an accepted solution will be posted when the next week's challenge is posted.

Challenges will be located in the Challenges folder

## How to submit

Create a folder in the submissions folder that is your username, and then create a subfolder named after the challenge

Example: `Submissions/eragon5779/Chinese Zodiac`

Put all files in there for your code to run.

Any programming language is allowed.

## Challenges

The challenge name and the week will be posted here.

### May 13

[Chinese Zodiac](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/ChineseZodiac.md)

### May 21

[Harshad Series](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/HarshadSeries.md)

### May 27

[SHA-1 Implementation](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/SHA1Implentation.md)

### June 10

[Metronome](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/Metronome.md)

### June 24

[Guess the Number](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/GuessTheNumber.md)

### July 1

[Penney's Game](https://gitlab.com/tcunc/codechallenges/blob/master/Challenges/PenneysGame.md)
