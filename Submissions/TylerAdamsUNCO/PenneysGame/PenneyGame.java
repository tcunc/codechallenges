import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;
public class PenneyGame {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		Random rand = new Random();
		String playerSequence = "";
		String computerSequence = "";
		int firstOrSecond = rand.nextInt(1000);
		System.out.println("Hello! We're going to play Penney's Game.");
		if(firstOrSecond < 500) {
			computerSequence = randomCompChoice();
			System.out.printf("The computer will go first. It has chosen %s.%n", computerSequence);
		}
		else if(firstOrSecond > 500) {
			System.out.println("You will go first.");
		}
		System.out.println("You will choose the outcome of three coin flips and enter it in the format of HTH (Heads, Tails, Heads): ");
		playerSequence = kb.next();
		playerSequence = playerSequence.toUpperCase();
		while(choices(playerSequence) == false) {
			System.out.println("Invalid input. Please try again: ");
			playerSequence = kb.next();
			choices(playerSequence);
		}
		if(firstOrSecond > 500) {
			computerSequence = optimalCompChoice(playerSequence);
			System.out.printf("Now that you've chosen, the computer will choose. It has chosen %s.%n", computerSequence);
		}
		kb.close();
		System.out.println("We're now going to flip a coin repeatedly until either your or the computer's sequence appears.");
		System.out.println("Please press Enter after each flip.");
		coinToss(playerSequence, computerSequence);
	}

	public static boolean choices(String UserInput) {
		String[] allChoices = {"HHH", "HHT", "HTT", "HTH", "THH", "TTH", "THT", "TTT"};
		List<String> list = Arrays.asList(allChoices); 
		if(list.contains(UserInput)) return true;
		else return false;
	}
	
	public static String randomCompChoice() {
		Random rand = new Random();
		String choice = "";
		for(int i = 0; i < 3; i++) {
			int hOrT = rand.nextInt(1000);
			if(hOrT < 500) choice += "H";
			else choice += "T";	
		}
		return choice;
	}
	
	public static String optimalCompChoice(String playerInput) {
		String choice = "";
		if (playerInput.charAt(1) == 'H') choice += "T";
		else choice += "H";
		choice += playerInput.substring(0, 2);
		return choice;
	}
	
	public static void coinToss(String player, String computer) {
		boolean playerWins = false;
		boolean computerWins = false;
		Random rand = new Random();
		String sequence = "";
		while(!playerWins && !computerWins) {
			int hOrT = rand.nextInt(1000);
			if(hOrT < 500) sequence+="H";
			else sequence+="T";
			System.out.print(sequence);
			if(sequence.contains(player)) {
				playerWins = true;
				System.out.printf("%nPlayer has won! %s was flipped!", player);
				break;
			}
			if(sequence.contains(computer)) {
				computerWins = true;
				System.out.printf("%nComputer has won! %s was flipped!", computer);
				break;
			}
		}
	}
}
