﻿using System;
using System.Collections.Generic;

namespace ChineseZodiac
{
    class Program
    {
        public static void Main(string[] args)
        {
            int year = 0;
            Console.Write("Please Enter a Year Later Than 3: ");
            String input = Console.ReadLine();
            int.TryParse(input, out year);
            if(year >= 4) Console.WriteLine(CalcElement(year) + " " + CalcAnimal(year) + " " +  CalcYinYang(year));
            else Console.WriteLine("Error: Year entered must be greater than 3.");
        }
        
        public static string CalcYinYang(int year)
        {
            String YinOrYang = "";
            if(year %2 == 0) YinOrYang = "Yang"; //If the year is even
            else YinOrYang = "Yin"; //If the year is odd
            return YinOrYang;
        }

        public static string CalcAnimal(int year)
        {
            String Animal = "";
            int animalIndex = (year - 4) % 12; //-4 because the cycle began year 4
            List<String> AnimalList = new List<String>(){"Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"};
            Animal = AnimalList[animalIndex];
            return Animal;
        }

        public static string CalcElement(int year)
        {
            String Element = "";
            int elementIndex = (year - 4) % 5;
            List<String> ElementList = new List<String>(){"Wood", "Wood", "Fire", "Fire", "Earth", "Earth", "Metal", "Metal", "Water", "Water"};
            //I kinda hate that doubled array but each element goes twice in a row and that was the quickest way I could think to do so
            Element = ElementList[elementIndex];
            return Element;
        }
    }
}
