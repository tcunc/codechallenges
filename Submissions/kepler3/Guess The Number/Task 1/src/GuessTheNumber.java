package codeChallenge;

import java.util.Scanner;

//made by Kepler for the TCUNC Code Challenge June 25, 2019
public class GuessTheNumber {
	
	public static void main(String args[]) {
		//have the computer initialize the correct answer
		int answer = initializeAnswer();
		
		//set the win condition to false
		boolean win = false;
		
		//open a scanner to read input
		Scanner scanner = new Scanner(System.in);
		
		while(!win) {
		    //get the input from the player and check to see if they won
			String input = getInput(scanner);
			win = checkResult(answer, input);
		}//repeat until the player wins
		
		//close our scanner
		scanner.close();
		
		//exit the program
		System.exit(0);
	}
	
	//initializes the correct answer
	private static int initializeAnswer() {	
	    //display a message to let the player know what is going on
		System.out.println("The computer has chosen a number between 1 and 10.");

		//returns a number from 1 to 10 using Math.random
		//since Math.random ranges [0,1), we multiply by ten
		//to get a range [0,10) then use floor to get an int from 0 to 9
		//then add 1 to get 1 to 10
		return (int) Math.floor(Math.random()*10) + 1;		
	}
	
	//prompts the player for input
	//and returns the input value
	private static String getInput(Scanner scanner) {
		//prompt for input
		System.out.println("Please guess the number:");
		
		//wait for input and store it in the String input
		String input = scanner.next();	
		
		return input;
	}
	
	
	//checks a given input
	//displays an appropriate response
	//outputs:
	//	true, if the player has won
	//	false, otherwise
	private static boolean checkResult(int correctAnswer, String input) {
		try {
		    //we need to convert the String to int
		    //this is what needs the try-catch block
		    //as it could lead to a number format exception
			int inputInt = Integer.parseInt(input);
			
			//test the input to see if it matches the correct answer
			if(inputInt == correctAnswer) {
				//the correct answer was guessed, so output the prompt
				//and set the win condition to true
				System.out.println("Well guessed!");
				return true;
			} else {
				//the guess was wrong, output an appropriate message
				System.out.println("Incorrect!");
			}
		} catch (Exception e) {
		    //we got here if the string wasn't able to be converted to an int
		    //which means the player entered a non-integer
		    //display a warning
			System.out.println("Invalid Input!");
		}
		
		//we only get here if we didn't return true already for a correct answer,
		//so the player has not won yet.
		return false;
	}
}
