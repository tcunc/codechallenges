package codeChallenge;

import java.util.Scanner;

public class GuessTheNumber_3 {

    //sets the minimum and maximum values for the range
	final static int MIN = 0;
	final static int MAX = 99;

	public static void main(String args[]) {
		//open a scanner to read input
		Scanner scanner = new Scanner(System.in);
		
		//initialize the correct answer
		initializeAnswer(scanner);
		
		//set win condition to false
		boolean exit = false;
		
		//set initial values of min_remain and max_remain to MIN and MAX
		int min_remain = MIN;
		int max_remain = MAX;
		
		while(!exit) {
			//let the computer make a guess
			int inputInt = computerGuess(min_remain, max_remain);
			
			//result is an int array containing
			//[0]: exit with 1 for true
			//[1]: new minimum
			//[2]: new maximum
			int[] result = checkResult(scanner, inputInt, min_remain, max_remain);
			
			//set new max and min based on result from the scorer
			exit = (result[0]==1);
			min_remain = result[1];
			max_remain = result[2];
		}
		//close the scanner
		scanner.close();
		
		//exit the program
		System.exit(0);
	}
	
	//initializes the correct answer
	private static void initializeAnswer(Scanner scanner) {		
		//prompt the player to choose a number
		System.out.printf("Please choose a number between %d and %d and press enter when ready.\n",MIN,MAX);
		
		//read in next line and ignore it.
		//this signals that the user is ready to continue
		//and prevents the programming from guessing before the user is ready
		scanner.nextLine();
		
		return;
	}
	
	//prompts the computer for a guess
	//and returns the integer guessed
	private static int computerGuess(int min, int max) {
		//guess the median/average of the min and max
		int guess = (int) ((max + min) / 2);
		
		//print out guess
		System.out.printf("Computer guesses the number: %d\n",guess);
		
		return guess;
	}
	
	
	//checks a given input against the answer
	//displays an appropriate response
	//outputs an integer array:
	//	result[0]: equal to 1 if and only if the system has reached an exit condition
	//	result[1]: the new minimum value the chosen number could be
	//  result[2]: the new maximum value the chosen number could be
	private static int[] checkResult(Scanner scanner, int inputInt, int min_remain, int max_remain) {
		//initialize a boolean to indicate if a valid input was given
		boolean validInput = false;
		
		//initialize a return array
		int[] result = new int[3];
		result [0] = 0;
		result [1] = min_remain;
		result [2] = max_remain;
		
		//continue to ask for input until a valid input is given
		while(!validInput) {
			//prompt the scorer to rate the computer's guess
			System.out.println("Was the computer's guess low (L), high (H), or correct (C)");
			System.out.print("Enter L, H, or C: ");
			String input = scanner.next();
		
			if(input.length()==0 || input.length() > 1) {
				//input is too short or too long
				//prompt for input again
				System.out.println("Sorry! Your input must be a single character!");
			} else {
			    //input is exactly one character, so we can use a switch statement to see which one
				switch(input.charAt(0)) {
					case 'c':
					case 'C':
						//the correct answer was guessed, so output the prompt
						//and set the win condition to true
						System.out.println("Computer's guess was correct.");
						System.out.println("Thank you for playing.");
						
						//set exit condition to true (0) and valid input to true
						result[0]=1;
						validInput = true;
						break;
					case 'l':
					case 'L':
						System.out.println("Computer's guess was too low.");
						
						//set new minimum based on the guessed value and valid input to true
						result[1]=inputInt+1;
						validInput = true;
						
						break;
					case 'h':
					case 'H':
						System.out.println("Computer's guess was too high.");

						//set new maximum based on the guessed value and valid input to true
						result[2]=inputInt-1;
						validInput = true;
						
						break;
					default:
					    //any other character is invalid, so display an error to the scorer
						System.out.println("Please input one of the given characters!");
				}
			}
		}
		
		//test to see if the remaining possible integers is valid (contains at least one valid guess left)
		if(result[1]>result[2]) {
			//this indicates that the scorer has lied somewhere
			//and the possible range of values is empty
			
			//display an error and set exit status (result[0]) to true
			System.out.println("Error! No possible integer values for the answer remain!");
			System.out.println("Exiting program!");
			
			result[0] = 1;
		}
		
		//finally, return the result array
		return result;
	}
}
