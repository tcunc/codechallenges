package codeChallenge;

import java.util.Scanner;

public class GuessTheNumber_2 {
	
	//sets the minimum and maximum values for the range
	final static int MIN = 0;
	final static int MAX = 99;

	public static void main(String args[]) {
	    //initializes the correct answer
		int answer = initializeAnswer();
		
		//set the win condition to false
		boolean win = false;
		
		//open a scanner to read user input
		Scanner scanner = new Scanner(System.in);
		
		//loop until the player wins
		while(!win) {
		    //get input from the player for the guess
			String input = getInput(scanner);
			
			//check the player's guess
			win = checkResult(answer, input);
		}
		
		//close the scanner
		scanner.close();
		
		//exit the program
		System.exit(0);
	}
	
	//initializes the correct answer
	private static int initializeAnswer() {		
		System.out.printf("The computer has chosen a number between %d and %d.\n",MIN,MAX);

		//returns a number from MIN to MAX using Math.random
		//since Math.random ranges [0,1), we multiply by MAX-MIN+1
		//to get a range [0,MAX-MIN+1) then use floor to get an int from 0 to MAX-MIN
		//then add MIN to get a range from MIN to MAX, inclusive
		return (int) Math.floor(Math.random()*(MAX-MIN+1)) + MIN;
	}
	
	//prompts the player for input
	//and returns the input value
	private static String getInput(Scanner scanner) {
		//prompt for input
		System.out.println("Please guess the number:");
		
		//wait for input and store it in the String input
		String input = scanner.next();	
		
		return input;
	}
	
	
	//checks a given guess to see if it is correct
	//displays an appropriate response
	//outputs:
	//	true, if the player has won
	//	false, otherwise
	private static boolean checkResult(int correctAnswer, String input) {
		try {
		    //we need to convert the string to an integer to test it
		    //this is what needs the try-catch block
		    //as it could result in a number format exception
			int inputInt = Integer.parseInt(input);
			
			//test the input to see if it is correct
			if(inputInt == correctAnswer) {
				//the correct answer was guessed, so output the prompt
				//and set the win condition to true
				System.out.println("Your guess was correct.");
				System.out.println("Well guessed!");
				return true;
			} else {
				//the guess was wrong, output an appropriate message
				if(inputInt < correctAnswer){
					System.out.println("Your guess was too low.");
				} else {
					System.out.println("Your guess was too high.");
				}
				
			}
		} catch (Exception e) {
		    //this is a number format exception and means the user has entered a non-integer
		    //display an error
			System.out.println("Invalid Input!");
		}
		
		//we only get here if we didn't return true already for a correct answer,
		//so the player has not won yet.
		return false;
	}

}
