import time

while(True):
    
    animal = ["Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"]
    element = ["Wood", "Wood", "Fire", "Fire", "Earth", "Earth", "Metal", "Metal", "Water", "Water"]


    print("Welcome to Noah's Program to figure out your Chinese Zodiac")
    print("All calculations done will done from first AD cycle, 4 AD")
    inYear = input("Please input a year:")
    try:
        inYear = int(inYear)
    except:
        print("That's not a number!")
        continue

    
    if (inYear < 4):
        print("You Hooiser, you think you're funny aren't you? I said from 4 AD, please enter a number greater then or equal to that")
        continue

    
    inYear = (inYear - 4) % 60

    if (inYear % 2 == 0):
        yinYang = "Yang"
    else:
        yinYang = "Yin"

    yearAnimal = animal[(inYear % 12)]
    
    yearElement = element[(inYear % 10)]

    print("The Chinese Zodiac for that year is " + yinYang + " year of the " + yearElement + " " + yearAnimal)
    time.sleep(5)
    break