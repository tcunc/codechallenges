################################################################################
## Matthew Shortle
## Guess the Number
##
#################################GUESS THE NUMBER###############################

import random

secret = random.randrange(1, 10)

guess = True

while guess != secret:
    try:
        guess = int(input("Enter a number between 1 and 10: "))
    except ValueError as ex:
        print("Invalid input")
    if guess == secret:
        print("Well guessed!")
    elif guess < secret:
        print("The number is higher than your input.")
    elif guess > secret:
        print("The number is lower than your input.")
    elif guess > 11:
        print("Invalid input")
    else:
        print("Invalid input")


