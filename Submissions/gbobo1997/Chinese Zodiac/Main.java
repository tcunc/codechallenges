// replit link: https://repl.it/@gbobo1997/ChineseZodiac2
import java.util.Scanner;
class Main 
{
  public static void main(String[] args) 
  {
    String animals[] = new String[]{
      "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"
    };
    String elements[] = new String[]{
      "Wood", "Wood", "Fire", "Fire", "Earth", "Earth", "Metal", "Metal", "Water", "Water"
    };
    String associations[] = new String[]{
      "Yang", "Yin"
    };
    Scanner in = new Scanner(System.in);
    int year = 0;
    while(true)
    {
      System.out.println("\nEnter year: ");
      year = in.nextInt();
      if(year < 1984)
      { 
        year = 2044 - (1984-year);
        System.out.println("Year less than 1984, looping year.");
      }
      System.out.println(year+": "+elements[(year-1984)%5]+" "+animals[(year-1984)%12]+" "+associations[year%2]);
    }
    
  }
}