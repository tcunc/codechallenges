// https://repl.it/@gbobo1997/HarshadSeries
import java.util.Scanner;
import java.util.ArrayList;
class Main {
  public static void main(String[] args) 
  {
    Scanner in = new Scanner(System.in);
    ArrayList<Integer> nValues = new ArrayList<Integer>();
    System.out.println("Enter amount to generate");
    int n = in.nextInt();
    int i = 1;
    while(n > 0)
    {
      if(niven(i))
      {
        nValues.add(i);
        n--;
      }
      i++;
    }
    System.out.println("Values: ");
    for(Integer k: nValues)
    {
      System.out.print(k+" ");
    }
    System.out.print("\nHashad > 1000: ");
    i = 1000;
    while(true)
    {
      if(niven(i))
      {
        System.out.print(i);
        break;
      }
      i++;
    }
  }  
  public static boolean niven(int n)
  {
    char[] values = String.valueOf(n).toCharArray();
    int sum = 0;
    for(char c: values)
    {
      sum += Character.getNumericValue(c);
    }
    return n % sum == 0;
  }
}