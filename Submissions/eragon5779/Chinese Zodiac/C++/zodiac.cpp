#include <string>
#include <iostream>

const std::string animals[12] = {
                      "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake",
										  "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig"
                    };
const std::string elements[5] = { "Wood", "Fire", "Earth", "Metal", "Water" };
const std::string yangs[2] = { "Yang", "Yin" };
// The following character lists do not print properly, but are correctly chosen
const std::string stems[10] = { "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸" };
const std::string branches[12] = { "子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥" };

void zodiac(int year)
{
	if (year < 4)
	{
		std::cout << "Year " << year << " not valid. Please try again";
		return;
	}
	int cycle = (year - 4) % 60;
	std::string element = elements[(cycle / 2) % 5];
	std::string animal = animals[cycle % 12];
	std::string yang = yangs[cycle % 2];
	std::string stem = stems[cycle % 10];
	std::string branch = branches[cycle % 12];
	std::cout << element << " " << animal << " (" << yang << ")\n"
		<< stem << branch << "\n" << cycle + 1 << "/60\n";
}

void zodiac(std::string year)
{
	int iyear = std::stoi(year);
	zodiac(iyear);
}

int main()
{
  std::cout << "\nEnter Year: ";
  std::string year;
  std::cin >> year;
  zodiac(year);
  return 0;
}
