animals = [
    "Rat",
    "Ox",
    "Tiger",
    "Rabbit",
    "Dragon",
    "Snake",
    "Horse",
    "Goat",
    "Monkey",
    "Rooster",
    "Dog",
    "Pig",
]
elements = ["Wood", "Fire", "Earth", "Metal", "Water"]
yang = ["Yang", "Yin"]
stems = ["甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"]
branches = ["子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"]
annex = 4

try:
    while True:
        inp = input("Enter year: ")
        try:
            cycle = (int(inp) - 4) % 60
        except:
            print("Invalid year")
            continue
        print(
            f"{elements[(cycle//2)%5]} {animals[cycle%12]} ({yang[cycle%2]})\n{stems[cycle%10]}{branches[cycle%12]}\n{cycle+1}/60"
        )
except KeyboardInterrupt:
    print("^C")
    pass
