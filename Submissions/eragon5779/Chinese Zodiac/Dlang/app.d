import std.stdio, std.conv,
       std.math, std.string;

void main()
{
	string[] animals =  [ "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake",
                              "Horse", "Goat", "Monkey", "Rooster", "Dog", "Pig" ];
        string[] elements = [ "Wood", "Fire", "Earth", "Metal", "Water" ];
	string[] yang = [ "Yang", "Yin" ];
	string[] stems = [ "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸" ];
	string[] branches = [ "子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥" ];
	while (true) {
		writef("Enter year: ");
		int year;
		try {
			year = readln.strip.to!int;
		}
		catch (ConvException e) {
			writeln("Year not valid. Please try again");
			continue;
		}
		if (year < 4) { writeln("Year not valid. Please try again"); continue; }
		int cycle = (year-4)%60;
		writefln("%s %s (%s)\n%s%s\n%s/60", elements[round(cycle/2).to!int%5], animals[cycle%12],
	       		 yang[cycle%2], stems[cycle%10], branches[cycle%12], (cycle + 1).to!string);
	}
}
