#include <stdio.h>
#include <ctype.h>

int digisum(int num) {
  int n = num, sum = 0;
  while (n != 0) {
    sum += n % 10;
    n = n / 10;
  }
  return sum;
}

int harshad(int num) {
  if (num % digisum(num) == 0) {
    return 1;
  }
  return 0;
}

int main() {

  for (;;) {
    int x;
    int y;
    printf("Enter x: ");
    int xresult = scanf_s("%d", &x);
    if (xresult == 0) {
      while (fgetc(stdin) != '\n');
      printf("Please enter numbers for x and y\n");
      continue;
    }
    printf("Enter y: ");
    int yresult = scanf_s("%d", &y);
    if (yresult == 0) {
      while (fgetc(stdin) != '\n');
      printf("Please enter numbers for x and y\n");
      continue;
    }
    if (xresult == EOF || yresult == EOF) {
      printf("Please enter numbers for x and y\n");
      continue;
    }
    for (int i = 0; i < x; i++) {
      if (digisum(i) == y) {
        if (harshad(i)) {
          printf("%d\n", i);
        }
      }
    }
  }
  return 0;
}
