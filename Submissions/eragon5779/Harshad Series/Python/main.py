def digisum(num):
    return sum([int(x) for x in str(num)])


def harshad(num):
    if num % digisum(num) == 0:
        return True
    return False


while True:
    try:
        x = int(input("Enter x: "))
        y = int(input("Enter y: "))
    except ValueError:
        print("Please enter numbers for x and y")
        continue
    except KeyboardInterrupt:
        print("^C")
        break
    for i in range(0, x + 1):
        if digisum(i) == y:
            if harshad(i):
                print(i)
