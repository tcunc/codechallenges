import std.stdio, std.algorithm, std.range, std.conv, std.string, std.array;

void main() {
  enum digisum = (int n) => n.text.map!(d => d - '0').sum;
  enum harshad = (int x, int y) => iota(1, x).filter!(n => n % digisum(n) == 0 && digisum(n) == y);
  while (true) {
    writef("Enter x: ");
    const int inx = readln.strip.to!int;
    writef("Enter y: ");
    const int iny = readln.strip.to!int;
    harshad(inx, iny).map!(to!string).join("\n").writeln;
  }
}
