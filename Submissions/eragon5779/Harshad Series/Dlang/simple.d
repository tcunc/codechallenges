import std.stdio, std.conv, std.string, std.algorithm;

int digisum(int num)
{
  int n = num, sum = 0;
  while (n != 0)
  {
    sum += n % 10;
    n /= 10;
  }
  return sum;
}

bool harshad(int num)
{
  if (num % digisum(num) == 0)
  {
    return true;
  }
  return false;
}

void main()
{
  while (true)
  {
    int x;
    int y;
    try
    {
      writef("Enter x: ");
      x = readln.strip.to!int;
      writef("Enter y: ");
      y = readln.strip.to!int;
    }
    catch (ConvException e)
    {
      writeln("Please enter numbers for x and y");
      continue;
    }
    for (int i = 0; i < x; i++)
    {
      if (digisum(i) == y)
      {
        if (harshad(i))
        {
          writeln(i);
        }
      }
    }
  }
}
