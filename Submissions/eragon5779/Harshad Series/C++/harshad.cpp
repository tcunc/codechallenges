#include <vector>
#include <iostream>

bool harshad(int test_num, int total)
{
	int sum = 0;
	for (int i = test_num; i > 0; i /= 10)
		sum += i % 10;
	if (total == -1 && test_num % sum == 0)
		return true;
	else if (total > 0 && test_num % total == 0)
		return true;
	return false;
}

std::vector<int> harshad_range(int max, int sum)
{
	std::vector<int> values;
	for (int i = 1; i < max; i++)
	{
		if (harshad(i, sum))
			values.push_back(i);
	}
	return values;
}

int main()
{
  int max, sum;
  std::cout << "\nMax Value: ";
  std::cin >> max;
  std::cout << "\nSum Value: ";
  std::cin >> sum;
  std::vector<int> values = harshad_range(max, sum);
  std::cout << "\nAll Harshad numbers less than " << max << " with sum of " << sum << ":\n";
	for (int& i : values)
		std::cout << i << " ";
  return 0;
}
