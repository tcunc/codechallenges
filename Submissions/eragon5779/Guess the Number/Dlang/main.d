import std.stdio, std.conv, 
	   std.string;
import std.math: floor;
import std.random: Random, uniform;
import std.algorithm: canFind;
import std.range: iota;

int get_input(string prompt, int max = 0x7FFFFFFF)
{
	int tmp = -1;
	while (true)
	{
		try
		{
			writef(prompt);
			tmp = readln.strip.to!int;
			if (tmp < 0 || tmp > max)
				throw new ConvException("Out of Range during conversion");

			break;
		}
		catch (ConvException e) 
			writeln("Please enter a valid number");
	}
	return tmp;
}

void cpuguess() 
{
	string[] verify = ["<", "=", ">"];
	int limit = get_input("Enter a max number: ");
	int interval = floor(limit / 2.0).to!int;
	int current_guess = interval;
	while (true) 
	{
		writef("Is %d the correct number? [<, =, >] ", current_guess);
		string hilo = readln.strip;
		if (!verify.canFind(hilo)) 
			writeln("Please verify if the guess is less than, equal to, or greater than your number.");
		
		else
		{
			interval = floor(interval / 2.0).to!int;
			if (hilo == "=")
			{
				writeln("Yay! I guessed it!");
				break;
			}
			else if (hilo == "<")
				current_guess -= interval;
			
			else if (hilo == ">")
				current_guess += interval;
			
			if (interval < 2)
				interval = 2;
		}
	}		
}

void cpufeedback()
{
	int limit = get_input("Enter a max number: ");
	auto rnd = Random(limit);
	int num = uniform(1, limit+1, rnd);
	while (true)
	{
		int guess = get_input("Enter a number between 1 and %d: ".format(limit), limit);
		if (guess == num)
		{
			writeln("Well guessed!");
			break;
		}
		else if (guess > num)
			writeln("Your guess (%s) is greater than the number!".format(guess));
		else if (guess < num)
			writeln("Your guess (%s) is less than the number!".format(guess));
	}
}

void base() 
{
	auto rnd = Random();
	int num = uniform(1, 11, rnd);
	while (true)
	{
		int guess = get_input("Enter a number between 1 and 10: ", 10);
		if (guess == num)
		{
			writeln("Well guessed!");
			break;
		}
		else
			writeln("Incorrect!");
	}
}

void main()
{
	writeln("Welcome to Guess the Number!");
	bool running=true;
	while(running) {
		writeln("1) Base\n2) With Feedback\n3) CPU Guess\n4) Quit");
		int menu_choice = -1;
		try
		{
			writef("Which gamemode? ");
			menu_choice = readln.strip.to!int;
			if (menu_choice > 4 || menu_choice < 1)
			{
			throw new ConvException("Out of Range during conversion");
			}
		}
		catch (ConvException e)
		{
			writeln("Choice not valid. Please try again.");
			continue;
		}
		switch (menu_choice) {
			case 1: base(); break;
			case 2: cpufeedback(); break;
			case 3: cpuguess(); break;
			default: running=false; break;
		}
	}
	writeln("Goodbye!");
}
