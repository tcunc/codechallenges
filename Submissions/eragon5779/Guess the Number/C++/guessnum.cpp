#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>

int get_input(std::string prompt, int max  = 0x7FFFFFFF)
{
	int tmp = -1;
	while (true)
	{
		std::cout << prompt;
		std::cin >> tmp;
		if (tmp < 0 || tmp > max)
			std::cout << "\nPlease enter a valid number\n";
		else
			break;
	}
	return tmp;
}

void cpuguess()
{
	std::string verify = "<=>";
	int limit = get_input("\nEnter a max number: ");
	int interval = limit / 2;
	int curr_guess = interval;
	while (true)
	{
		std::cout << "\nIs " << curr_guess << " the correct number? [<, =, >] ";
		std::string hilo;
		std::cin >> hilo;
		if (verify.find(hilo) == std::string::npos)
		{
			std::cout << "Please verify if the guess is less than, "
				<< "equal to, or greater than your number.";
		}
		else
		{
			interval /= 2;
			if (hilo == "=")
			{
				std::cout << "Yay! I guessed it!";
				break;
			}
			else if (hilo == "<")
			{
				curr_guess -= interval;
			}
			else if (hilo == ">")
			{
				curr_guess += interval;
			}
			if (interval < 2)
				interval = 2;
		}
	}
}

void cpufeedback()
{
	int limit = get_input("\nEnter a max number: ");
	std::string prompt = "\nEnter a number between 1 and " + std::to_string(limit) + ": ";
	std::srand(std::time(NULL));
	int num = std::rand() % limit + 1;
	while (true)
	{
		int guess = get_input(prompt, limit);
		if (guess == num)
		{
			std::cout << "\nWell guessed!";
			break;
		}
		else if (guess > num)
			std::cout << "\nYour guess (" << std::to_string(guess) << ") is too high!";
		else if (guess < num)
			std::cout << "\nYour guess (" << std::to_string(guess) << ") is too low!";
	}
}

int main()
{
  bool running = true;
  while (running)
  {
    std::cout << "\n1) With Feedback\n2) CPU Guess\n3) Quit\n";
    int choice = -1;
    std::cout << "Select gamemode: ";
    std::cin >> choice;
    if (choice > 3 || choice < 1)
    {
      std::cout << "\nChoice not valid. Please try again.\n";
      continue;
    }
    switch (choice) {
      case 1: cpufeedback(); break;
      case 2: cpuguess(); break;
      default: running = false; break;
    }
  }
  std::cout << "\nGoodbye!\n";
  return 0;
}
