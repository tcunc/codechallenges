from random import SystemRandom as SR


def get_input(prompt, m=9223372036854775807):
    while True:
        tmp = input(prompt)
        try:
            tmp = int(tmp)
            if tmp > m or tmp <= 0:
                raise Exception
            return tmp
        except:
            print(f"{tmp} is not valid. Please try again")


def cpuguess():
    limit = get_input("Enter a max number: ")
    interval = limit // 2
    current_guess = limit // 2
    while True:
        inp = input(f"Is {current_guess} your number? [<, =, >] ")
        if not inp in ["<", "=", ">"]:
            print(
                "Please verify if guess is less than, equal to, or greater than your number"
            )
        if inp == "=":
            print("Yay! I guessed it!")
            break
        elif inp == "<":
            interval = interval // 2
            current_guess -= interval
        elif inp == ">":
            interval = interval // 2
            current_guess += interval
        if interval in [0, 1]:
            interval = 2


def cpufeedback():
    limit = get_input("Enter a max number: ")
    number = SR().randint(1, limit)
    while True:
        inp = int(get_input(f"Enter a number between 1 and {limit}: ", limit))
        if inp == number:
            print(f"Well guessed!")
            break
        else:
            if inp > number:
                print(f"Your guess ({inp}) is greater than the number")
            else:
                print(f"Your guess ({inp}) is less than the number")


def base():
    number = SR().randint(1, 10)
    while True:
        inp = int(get_input("Enter a number between 1 and 10: ", 10))
        if inp == number:
            print(f"Well guessed!")
            break
        else:
            print("Incorrect!")


dynamic_choice = [base, cpufeedback, cpuguess]


if __name__ == "__main__":
    try:
        while True:
            print("1) Base\n2) CPU Feedback\n3) CPU Plays\n4) Quit")
            inp = input("Please select gamemode: ")
            if inp == "4":
                print("\nGoodbye!")
                exit()
            elif inp not in ["1", "2", "3"]:
                print(f"{inp} is not a valid gamemode")
            else:
                dynamic_choice[int(inp) - 1]()
    except KeyboardInterrupt:
        print("\n\nGoodbye!")
