import std.stdio, std.string, std.conv,
	   std.regex, std.array, std.algorithm;
import std.random: Random, uniform;

char[3] sequence;
char[3] player_choice;
char[3] cpu_choice;
int[2] score = [0,0]; // Player, CPU

void main() {
	/* Test code
	player_choice = ["H","T","T"];
	cpu_choice = ["T", "H", "H"];
	play();
	*/
	// Actual code
	while (true) {
		writef("Would you like to go first? (y/n) ");
		string inp = readln.strip;
		if (!["y","n"].canFind(inp)) {
			writeln("\nPlease enter y or n");
			continue;
		}
		bool smart = (inp == "y");
		if (smart) {
			get_player_choice(smart);
			get_cpu_choice(smart);
		}
		else {
			get_cpu_choice(smart);
			get_player_choice(smart);
		}
		play();
		writeln("\nPlayer: %d\n   CPU: %d\n".format(score[0], score[1]));
		writef("Would you like to play again? (y/n) ");
		inp = readln.strip;
		if (inp != "y")
			break;
	}
	writeln("Thanks for playing!");
}

void get_player_choice(bool smart) {
	if (!smart) {
		writeln("CPU Choice: %(%c%)".format(cpu_choice));
	}
	while (true) {
		writef("\nPlease input your choice ('HHT', 'THT', etc): ");
		string pchoice = readln.strip;
		if (!matchFirst(pchoice, regex(r"^[HT]{3}$"))) {
			writeln("\nInput not valid. Please try again");
			continue;
		}
		else {
			player_choice = pchoice.dup;
			break;
		}
	}
}

void get_cpu_choice(bool smart) {
	if (smart) {
		cpu_choice[0] = (player_choice[1] == 'H') ? 'T' : 'H';
		cpu_choice[1] = player_choice[0];
		cpu_choice[2] = player_choice[1];
	}
	else {
		for (int i = 0; i < 3; i++) {
			if (uniform(0,2) == 0)
				cpu_choice[i] = 'T';
			else
				cpu_choice[i] = 'H';
		}
	}
}

void play() {
	int counter = 0;
	writef("Coin flips: ");
	while (!check_win) {
		counter++;
		flip_coin();
		write(sequence[2]);
	}
	writeln("Took %d flips".format(counter));
}

bool check_win() {
	if (cpu_choice == sequence) {
		writeln("\nCPU Won!");
		score[1]++;
		return true;
	}
	else if (player_choice == sequence) {
		writeln("\nPlayer won!");
		score[0]++;
		return true;
	}
	return false;
}

void flip_coin() {
	shift_left();
	if (uniform(0,2) == 0)
		sequence[2] = 'T';
	else
		sequence[2] = 'H';
}

void shift_left() {
	sequence[0] = sequence[1];
	sequence[1] = sequence[2];
	sequence[2] = '\xFF';
}
