#include <iostream>
#include <regex>
#include <cstdlib>
#include <ctime>
#include <string>

char sequence[4] = { '\xFF','\xFF','\xFF','\0' };
char player_choice[4] = { '\xFF','\xFF','\xFF','\0' };
char cpu_choice[4] = { '\xFF','\xFF','\xFF','\0' };
int score[2] = { 0, 0 };

void get_player_choice(bool smart)
{
	if (!smart)
	{
		std::cout << "\nCPU Choice: " << cpu_choice;
	}
	while (true)
	{
		std::cout << "\nPlease input your choice ('HHT', 'THT', etc) ";
		std::string choice;
		std::cin >> choice;
		std::regex match("^[HT]{3}$");
		if (!std::regex_match(choice, match))
		{
			std::cout << "\nInput not valid. Please try again";
			continue;
		}
		else
		{
			choice.copy(player_choice, choice.size());
			break;
		}
	}
}

void get_cpu_choice(bool smart)
{
	if (smart)
	{
		cpu_choice[0] = (player_choice[1] == 'H') ? 'T' : 'H';
		cpu_choice[1] = player_choice[0];
		cpu_choice[2] = player_choice[1];
	}
	else
	{
		for (int i = 0; i < 3; i++)
			cpu_choice[i] = (rand() % 2 == 0) ? 'T' : 'H';
	}
}

bool check_win()
{
	if (std::strcmp(cpu_choice, sequence) == 0)
	{
		std::cout << "\nCPU Won!\n";
		score[1]++;
		return true;
	}
	else if (std::strcmp(player_choice, sequence) == 0)
	{
		std::cout << "\nPlayer Won!\n";
		score[0]++;
		return true;
	}
	return false;
}


void shift_left()
{
	sequence[0] = sequence[1];
	sequence[1] = sequence[2];
	sequence[2] = '\xFF';
}

void flip_coin()
{
  shift_left();
  if (rand() % 2 == 0)
  sequence[2] = 'T';
  else
  sequence[2] = 'H';
}

void play()
{
  std::srand(std::time(NULL));
  int counter = 0;
  std::cout << "Coin Flips: ";
  while (!check_win())
  {
    counter++;
    flip_coin();
    std::cout << sequence[2];
  }
  std::cout << "\nTook " << std::to_string(counter) << " flips\n";
}

void reset()
{
  for (int i = 0; i < 3; i++)
  {
    sequence[i] = '\xFF';
    player_choice[i] = '\xFF';
    cpu_choice[i] = '\xFF';
  }
}

int main()
{
	while (true)
	{
    reset();
		std::cout << "\nWould you like to go first? (y/n) ";
		std::string choice;
		std::cin >> choice;
		std::string valid = "yn";
		if (valid.find(choice) == std::string::npos)
		{
			std::cout << "\nPlease enter y or n";
			continue;
		}
		bool smart = (choice == "y");
		if (smart)
		{
			get_player_choice(smart);
			get_cpu_choice(smart);
		}
		else
		{
			get_cpu_choice(smart);
			get_player_choice(smart);
		}
		play();
		std::cout << "\nPlayer: " << score[0] << "    CPU: " << score[1];
		std::cout << "\nWould you like to play again? (y/n) ";
		std::cin >> choice;
		if (choice != "y")
			break;
	}
	std::cout << "\nThanks for playing!\n";
  return 0;
}
