import signal
import sys
import os
import pickle
import random
import string
from datetime import datetime as dt
import json

print_results = True

valid_chars = string.ascii_letters + " "

answer = "Alas poor Yorick"

guess_len = len(answer)

full_random = False

guess_list = []


def signal_handler(sig, frame):
    new_file = f"monkeys.pickle"
    with open(new_file, "wb") as f:
        pickle.Pickler(f).dump(guess_list)
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


def check_blind(guess: str):
    correct = [False] * guess_len
    for i in range(guess_len):
        if guess[i] == answer[i]:
            correct[i] = True
    return correct


def generate(guess, correct):
    for x in range(guess_len):
        if not correct[x]:
            guess[x] = random.choice(valid_chars)
    return guess


def check_all_correct(correct):
    return all(x == True for x in correct)


def blind_guess():
    global guess_list
    print_iter = 100000 if full_random else 100
    current_guess = [""] * guess_len
    current_correct = [False] * guess_len
    all_correct = False
    guess_count = len(guess_list)
    max_correct = 0
    if len(guess_list) > 0:
        current_guess = guess_list[-1]
        current_correct = check_blind(current_guess)
        all_correct = check_all_correct(current_correct)
    while not all_correct:
        correct_amt = len([x for x in current_correct if x == True])
        if (
            guess_count % print_iter == 0
            and guess_count > 0
            and not full_random
            and print_results
        ):
            print(
                f"{guess_count} "
                + f"({len([x for x in current_correct if x == True])}/{guess_len}): "
                + "".join(current_guess)
            )
        elif full_random and correct_amt > max_correct and print_results:
            print(
                f"New max at guess {guess_count} ({correct_amt}/{guess_len}): "
                + f"{''.join(current_guess)}"
            )
        elif full_random and correct_amt > max_correct:
            print(
                f"New max at guess {guess_count} ({correct_amt}/{guess_len})"
            )
        if full_random:
            current_guess = generate(current_guess, [False] * guess_len)
        else:
            guess_list.append(current_guess)
            current_guess = generate(current_guess, current_correct)
        if correct_amt > max_correct:
            max_correct = correct_amt
        current_correct = check_blind(current_guess)
        all_correct = check_all_correct(current_correct)

        guess_count += 1
    print(f"Found correct string after {len(guess_list)} tries")
    if print_results:
        print(f"{''.join(current_guess)}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            if arg.startswith("random="):
                if arg.split("=")[1].lower() in ["t", "true", "y", "yes"]:
                    full_random = True
            elif arg.startswith("state="):
                old_file = arg.split("=")[1]
                if os.path.isfile(old_file):
                    print(f"Loading state from {old_file}...")
                    with open(old_file, "rb") as f:
                        guess_list = pickle.Unpickler(f).load()
                    print("State loaded.")
            elif arg == "bee":
                print("Loading Bee Movie script...")
                with open("bee_movie_script.txt", "r") as f:
                    answer = f.read()
                guess_len = len(answer)
                charlist = []
                for c in answer:
                    if c not in charlist:
                        charlist.append(c)
                valid_chars = "".join(charlist)
                print_results = False
                print("Loaded Bee Movie")
            elif arg == "potter":
                print("Loading Harry Potter and the Sorcerer's Stone...")
                with open("sorcerers_stone.txt", "r") as f:
                    answer = f.read()
                guess_len = len(answer)
                charlist = []
                for c in answer:
                    if c not in charlist:
                        charlist.append(c)
                valid_chars = "".join(charlist)
                print_results = False
                print("Loaded Harry Potter and the Sorcerer's Stone")
            elif arg.startswith("phrase="):
                answer = arg.split("=")[1]
                print(f"Using custom phrase '{answer}'")
                guess_len = len(answer)
                valid_chars = (
                    string.ascii_letters
                    + string.punctuation
                    + string.digits
                    + "\n "
                )
            elif arg.startswith("file="):
                fname = arg.split("=")[1]
                print(f"Loading file {fname}...")
                try:
                    with open(fname, "r") as f:
                        answer = f.read()
                    guess_len = len(answer)
                    charlist = []
                    for c in answer:
                        if c not in charlist:
                            charlist.append(c)
                    valid_chars = "".join(charlist)
                    print_results = False
                    print(f"Loaded {fname}")
                except:
                    print("Could not load file! Using default text")
    print(f"Starting at {dt.now().isoformat()}")
    blind_guess()
    print(f"Finished at {dt.now().isoformat()}")
