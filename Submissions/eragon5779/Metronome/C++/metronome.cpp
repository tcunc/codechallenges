#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <atomic>
#include <windows.h>
#include <mmsystem.h>
#include <playsoundapi.h>

int bpm = 120;
int playing = true;

int bpm_to_ms(int bpm)
{
	return 60000 / bpm;
}

void play()
{
	int counter = 0;
	while (playing)
	{
		if (counter == 0)
		{
			PlaySound(TEXT("metronome-high.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}
		else
		{
			PlaySound(TEXT("metronome-low.wav"), NULL, SND_FILENAME | SND_ASYNC);

		}
		std::this_thread::sleep_for(std::chrono::milliseconds(bpm_to_ms(bpm)));
		counter = (counter + 1) % 4;
	}
}

int main()
{
	std::cout << "Type in a BPM to change to BPM, or Q to quit\n";
	std::thread metronome(play);
	while (true)
	{
		std::string input;
		std::cin >> input;
		if (input == "q" || input == "Q")
		{
			playing = false;
			break;
		}
		bpm = std::stoi(input);
	}
	metronome.join();
}
