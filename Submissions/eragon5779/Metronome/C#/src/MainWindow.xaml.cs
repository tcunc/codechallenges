﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Metronome
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Media.SoundPlayer high = new System.Media.SoundPlayer(Properties.Resources.metronome_high);
        private System.Media.SoundPlayer low = new System.Media.SoundPlayer(Properties.Resources.metronome_low);
        private object t_lock = new object();
        private bool t_stopRequested = false;
        private bool IsPlaying = false;
        public int Tempo { get; set; }
        public string Count { get; set; }
        public MainWindow()
        {
            Tempo = 120;
            Count = "4";
            InitializeComponent();
        }

        private void BtnToggle_Click(object sender, RoutedEventArgs e)
        {
            lock (t_lock)
            {
                if (!IsPlaying)
                {
                    btnToggle.Content = "Stop";
                    t_stopRequested = false;
                    IsPlaying = true;
                    ThreadPool.QueueUserWorkItem(ClickMethod);
                }
                else
                {
                    btnToggle.Content = "Play";
                    t_stopRequested = true;
                }
            }
        }

        private void ClickMethod(object state)
        {
            try
            {
                int step = 0;
                while (!t_stopRequested)
                {
                    if (step == 0)
                        high.Play();
                    else
                        low.Play();
                    int count;
                    step = (step + 1) % (int.TryParse(Count, out count) ? count : 4);
                    Thread.Sleep(60000 / Tempo);
                }
            }
            finally
            {
                lock(t_lock)
                {
                    IsPlaying = false;
                }
            }
        }

        private void TxtCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
