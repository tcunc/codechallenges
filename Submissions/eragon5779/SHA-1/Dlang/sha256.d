import std.stdio, std.string, std.conv, std.algorithm, std.format, std.array,
       std.range, std.typecons;

struct SHA256_CTX {
 char[64] data;
 uint datalen;
 ulong bitlen;
 uint[8] state;
};

int shl(int n, int b) { return (n << b); }
long shr(long n, long b) { return (n >> b); }
int shr(int n, int b) { return (n >> b); }
int rotl(int n, int b) { return shl(n, b) | (n >>> (32 - b)); }
long rotr(long n, int b) { return shr(n, b) | (n << (64-b)); }
int rotr(int n, int b) { return shr(n, b) | (n << (32-b)); }
int ch(int a, int b, int c) { return (a & b) ^ (~a & c); }
int maj(int a, int b, int c) { return (a & b) ^ (a & c) ^ (b & c); }
int S0(int n) { return rotr(n,2) ^ rotr(n,13) ^ rotr(n,22); }
int S1(int n) { return rotr(n,6) ^ rotr(n,11) ^ rotr(n,25); }
int S2(int n) { return rotr(n,7) ^ rotr(n,18) ^ shr(n,3); }
int S3(int n) { return rotr(n, 17) ^ rotr(n,19) ^ shr(n,10); }

ulong btol(string bin) {
  ulong total = 0;
  foreach (b; bin) {
    total <<= 1;
    (b == '1') ? total += 1 : total;
  }
  return total;
}

static const uint[] k = [
  0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
  0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
  0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
  0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
  0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
  0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
  0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
  0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
];

void sha256_transform(ref SHA256_CTX ctx, const char[] data) {
  uint a, b, c, d, e, f, g, h, i, j, t1, t2;
  uint[64] words;
  for (i = 0, j = 0; i < 16; i++, j += 4) {
    words[i] = (data[j] << 24) | (data[j+1] << 16) | (data[j+2] << 8) | data[j+3];
  }
  for ( ; i < 64; i++) {
    words[i] = S1(words[i-2]) + words[i-7] + S0(words[i-15]) + words[i-16];
  }
  a = ctx.state[0];
  b = ctx.state[1];
  c = ctx.state[2];
  d = ctx.state[3];
  e = ctx.state[4];
  f = ctx.state[5];
  g = ctx.state[6];
  h = ctx.state[7];
  for (i = 0; i < 64; i++) {
    t1 = h + S3(e) + ch(e, f, g) + k[i] + words[i];
    t2 = S2(a) + maj(a, b, c);
    h = g;
		g = f;
		f = e;
		e = d + t1;
		d = c;
		c = b;
		b = a;
		a = t1 + t2;
  }
  ctx.state[0] += a;
  ctx.state[1] += b;
  ctx.state[2] += c;
  ctx.state[3] += d;
  ctx.state[4] += e;
  ctx.state[5] += f;
  ctx.state[6] += g;
  ctx.state[7] += h;
}

void sha256_init(ref SHA256_CTX ctx) {
  ctx.datalen = 0;
  ctx.bitlen = 0;
  ctx.state[0] = 0x6a09e667;
  ctx.state[1] = 0xbb67ae85;
  ctx.state[2] = 0x3c6ef372;
  ctx.state[3] = 0xa54ff53a;
  ctx.state[4] = 0x510e527f;
  ctx.state[5] = 0x9b05688c;
  ctx.state[6] = 0x1f83d9ab;
  ctx.state[7] = 0x5be0cd19;
}

void sha256_update(ref SHA256_CTX ctx, const char[] data, size_t len) {
  uint i;
  for (i = 0; i < len; i++) {
    ctx.data[ctx.datalen] = data[i];
    ctx.datalen++;
    if (ctx.datalen == 64) {
      sha256_transform(ctx, ctx.data);
      ctx.bitlen += 512;
      ctx.datalen = 0;
    }
  }
}

void sha256_final(ref SHA256_CTX ctx, out char[32] hash) {
  uint i;
  i = ctx.datalen;

  if (ctx.datalen < 56) {
    ctx.data[i++] = 0x80;
    while (i < 56) {
      ctx.data[i++] = 0x00;
    }
  }
  else {
    ctx.data[i++] = 0x80;
    while (i < 64) {
      ctx.data[i++] = 0x00;
    }
    sha256_transform(ctx, ctx.data);
    foreach(x; iota(0,56)) {
      ctx.data[x] = 0.to!char;
    }
  }

  ctx.bitlen += ctx.datalen * 8;
  ctx.data[63] = (ctx.bitlen).to!char;
  ctx.data[62] = (ctx.bitlen >> 8).to!char;
  ctx.data[61] = (ctx.bitlen >> 16).to!char;
  ctx.data[60] = (ctx.bitlen >> 24).to!char;
  ctx.data[59] = (ctx.bitlen >> 32).to!char;
  ctx.data[58] = (ctx.bitlen >> 40).to!char;
  ctx.data[57] = (ctx.bitlen >> 48).to!char;
  ctx.data[56] = (ctx.bitlen >> 56).to!char;
  sha256_transform(ctx, ctx.data);

  for (i = 0; i < 4; ++i) {
    hash[i] = (ctx.state[0] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 4] = (ctx.state[1] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 8] = (ctx.state[2] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 12] = (ctx.state[3] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 16] = (ctx.state[4] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 20] = (ctx.state[5] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 24] = (ctx.state[6] >> (24 - i * 8)) & 0x000000ff;
    hash[i + 28] = (ctx.state[7] >> (24 - i * 8)) & 0x000000ff;
  }
}

void main() {
  char[] str = "a".dup;
  SHA256_CTX ctx;
  char[32] hash;
  sha256_init(ctx);
  sha256_update(ctx, str, str.length.to!int);
  sha256_final(ctx, hash);
  string outp = "";
  foreach (c; hash) {
    outp ~= "%x".format(c);
  }
  writeln(outp);
}
