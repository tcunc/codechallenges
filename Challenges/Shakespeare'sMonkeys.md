# Shakespeare's Monkeys
Ever heard the theory that given infinite monkeys on infinite typewriters, one of them will eventually produce Shakespeare? Time to code those monkeys.

### Task
Your mission, should you choose to accept it, is to write a program that will randomly generate a series of letters and spaces into a 16 character string, and then compare that string to a quote by Shakespeare.

Your quote is: "Alas poor Yorick". You may choose to ignore case.

Your program must generate the letters randomly, but you may do that in any way you choose.
Your program should keep score for how often the randomly generated strings match up letters.
For example: my program generates "tlcma pomz nrp g". My score would be 3/16, or roughly 19% correct.
Every 1,000 attempts, your program must return the closest matching string it has found so far, that string's score, and what attempt that string was found on.
If your program generates the correct string, it will return the amount of tries it took to write Shakespeare, and will exit.

### Bonus Mission
To improve upon your monkeys, we're going to have them gain some memory.
Design an algorithm that keeps any correct characters in the guess and only randomizes any incorrect characters.
As before, return your best string every 1,000 attempts and return the final amount of tries it took to write Shakespeare.