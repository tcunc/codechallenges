# SHA-1 Implementation

[Wiki Article](https://en.wikipedia.org/wiki/SHA-1)

SHA-1 is a message digest algorithm typically used for file verification and numerous forms of security, like TLS, SSL, SSH, S/MIME, and PGP. The algorithm takes an input and produces a 160-bit (20-byte) hash. Typically, this is rendered as a 40-digit hexadecimal string.

While SHA-1 is no longer used to due hash collisions and its insecurities, it is good for learning and academic purposes.

## Task

Create a program that is capable of taking in text input, and returning the sha1sum of the text. You may use internal methods to verify the returned sum, but only in that capacity (i.e. Python's `hashlib` library).

Optionally, implement a SHA-2 algorithm of your choice (resources below)

* [SHA-2 Wiki Article](https://en.wikipedia.org/wiki/SHA-2)
* [SHA-2 Wiki Pseudocode](https://en.wikipedia.org/wiki/SHA-2#Pseudocode)

## Requisite Information

There are a few variables that are set statically in the code. The basic outline of SHA-1 is listed in the Wikipedia article, specifically in the [pseudocode](https://en.wikipedia.org/wiki/SHA-1#SHA-1_pseudocode). The pseudocode should give you a general idea of how to implement the algorithm, as well as the static variables that are part of the algorithm.

## Example hashes

| Plaintext                                   | Hash                                     |
|:-------------------------------------------:|:----------------------------------------:|
| The TCUNC                                   |`55d5e028427e00f0909e4bcb1d039081b42e400c`|
| The TDUND                                   |`76fcff60cc39e7bcc69478bc88b3df23e58413bd`|
| The quick brown fox jumps over the lazy dog |`2fd4e1c67a2d28fced849ee1bb76e7391b93eb12`|

## Notes

TCBotV3 is able to hash text and files! To utilize this, type the following command into `#bot-spam` in the TCUNC:

`>hash <algo> <data>`

If you want to hash a file, just attach the file and type:

`>hash <algo>`

#### Source:

[TechCogsV3](https://gitlab.com/Eragon5779/TechCogsV3)
