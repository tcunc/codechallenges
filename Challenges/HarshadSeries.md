
# Harshad Series

The Harshad or Niven numbers are positive integers ≥ 1 that are divisible by the sum of their digits.

For example, `42` is a Harshad number as `42` is divisible by `(4 + 2)` without remainder. 

Assume that the series is defined as the numbers in increasing order.

## Task

The task is to create a function/method/procedure to generate successive members of the Harshad sequence.

Use it to list the first twenty members of the sequence and list the first Harshad number greater than 1000.

## Bonus Challenge

Given two inputs `x` and `y`, print all numbers less than `x` whose digit sum is `y`.

Example:

Given `x = 100` and `y = 6`, the expected output below would be printed:

```
6
24
42
60
```