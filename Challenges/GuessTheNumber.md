# Guess the Number
## Base
### Task
Write a program where the program chooses a number between   1   and   10.

A player is then prompted to enter a guess.   If the player guesses wrong,   then the prompt appears again until the guess is correct.

When the player has made a successful guess the computer will issue a   "Well guessed!"   message,   and the program exits.

A   conditional loop   may be used to repeat the guessing until the user is correct.

## With Feedback
### Task

Write a game (computer program) that follows the following rules:

* The computer chooses a number between given set limits.
* The player is asked for repeated guesses until the target number is guessed correctly
* At each guess, the computer responds with whether the guess is:
  * higher than the target,
  * equal to the target,
  * less than the target,   or
  * the input was inappropriate.

## With Player Feedback
### Task

Write a player for the game that follows the following rules:

* The scorer will choose a number between set limits.
* The computer player will print a guess of the target number.
* The computer asks for a score of whether its guess is higher than, lower than, or equal to the target.
* The computer guesses, and the scorer scores, in turn, until the computer correctly guesses the target number.

The computer should guess intelligently based on the accumulated scores given. One way is to use a Binary search based algorithm.


**A point will be awarded per task implemented, for up to 3 points**
